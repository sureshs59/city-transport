package com.city.service;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class CityServiceImpl implements CityServiceInterface{
	
	Logger log = LogManager.getLogger(this.getClass());
	
	/*
	Boston, New York
	Philadelphia, Newark
	Newark, Boston
	Trenton, Albany
	
	http://localhost:8080/connected?origin=Boston&destination=Newark
	Should return yes
	http://localhost:8080/connected?origin=Boston&destination=Philadelphia
	Should return yes
	http://localhost:8080/connected?origin=Philadelphia&destination=Albany
	Should return no
	*/
	
	public String isTransportAvailabilityBetweenCities(String origin,String destination,Map<String, String> cityMappings) {
		
		final String YES = "YES";
		log.info("==================VERIFYING Transportation between "+origin+" and "+destination);
		if( !StringUtils.isEmpty(origin) && !StringUtils.isEmpty(destination) ) {
			if( cityMappings.containsKey(origin) ) {
				for(Map.Entry<String, String> entry : cityMappings.entrySet()) {
					
					if( entry.getKey().equalsIgnoreCase(origin) ) {
						log.info("origin matching..."+origin);
						
						for(Map.Entry<String, String> subEntry : cityMappings.entrySet()) {
							
							log.info(" FIRST pass matching...subEntry.getValue()--"+subEntry.getValue()+"-------origin-----"+origin);
							if( subEntry.getValue().equalsIgnoreCase(origin) ) {
								String thirdPassKey = subEntry.getKey();
								log.info(" FIRST pass matching for KEY DESTINATION...subEntry.getKey()--"+subEntry.getKey()+"-------destination-----"+destination);
								if( destination.equalsIgnoreCase(thirdPassKey) ){
									log.info("FIRST PASS MATCHED: Direct transportation availability exist..between Origin:"+origin+" and destination : "+destination);
									return YES;
								}
								
								log.info(" SECOND pass failed so checking 2nd pass KEY with another values in map If it found then going to verify with destination for each Key");
								log.info(" --"+subEntry.getValue()+"-------thirdPassKey-----"+thirdPassKey);
								
								for(Map.Entry<String, String> thirdEntry : cityMappings.entrySet()) {
									if( thirdEntry.getValue().equalsIgnoreCase(thirdPassKey) ) {
										String thirdKey = thirdEntry.getKey();
										
										log.info(" Checking for SECOND pass matching...thirdKey:"+thirdKey+"----"+destination);
										if ( thirdKey.equalsIgnoreCase(destination) ) {
											log.info("SECOND PASS: Direct transportation availability exist..between Origin:"+origin+" and destination : "+destination);
											return YES;
										}
										
									}
								}
								
								if( subEntry.getValue().equalsIgnoreCase(thirdPassKey) ) {
									log.info(" THIRD pass matching for KEY DESTINATION...subEntry.getKey()--"+subEntry.getKey()+"-------destination-----"+destination);
									if( destination.equalsIgnoreCase(subEntry.getKey()) ){
										log.info("THIRD PASS MATCHED: Direct transportation availability exist..between Origin:"+origin+" and destination : "+destination);
										return YES;
									}
								}
							}
						}
					}
				}
				log.info(" Completed matching criteria.....");
			}
		}
		log.info(" Nothing is matched with Origin and destination criteria so returning No for origin: "
				+ ""+origin+" and destination :"+destination);
		return "NO";
	}

}
