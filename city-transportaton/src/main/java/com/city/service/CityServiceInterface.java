package com.city.service;

import java.util.Map;

import org.springframework.context.annotation.Configuration;

@Configuration
public interface CityServiceInterface {
	
	public String isTransportAvailabilityBetweenCities(String origin,String destination,Map<String,String> cityInfo);
	
}
