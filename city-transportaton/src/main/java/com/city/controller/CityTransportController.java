package com.city.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.city.service.CityServiceInterface;

@RestController
public class CityTransportController {

	Logger log = LogManager.getLogger(this.getClass());
	
	@Autowired
	CityServiceInterface cityService;

	@GetMapping("/connected")
	public String getTransportationBetweenCities(@RequestParam("origin") String origin,
			@RequestParam("destination") String destination) throws URISyntaxException, IOException {
		
		log.info("in getTransportationBetweenCities() method - Origin is : " + origin + "-- destination is: " + destination);
		final String fileName = "city.txt";
		// Load the City transport mapping file:
		
		String cityData = getFileContent(fileName);
			
		//Splitting the values based on next line and storing inside the String array
		String[] tranportCities = cityData.split("\n");

		Map<String, String> cityMappings = new HashMap<>();
		
		String[] arrayCityNames = null;
		
		for(int i =0 ; i<tranportCities.length; i++) {
			log.info("Tranported City information .."+tranportCities[i]);	
			
			arrayCityNames = tranportCities[i].split(",");
			
			for(int j =0 ; j<arrayCityNames.length; j++) {
				log.info("cityName.."+arrayCityNames[j]);	
				
				// Saving the city names in the HashMap as Key and Value pair.
				cityMappings.put(arrayCityNames[0].trim(), arrayCityNames[1].trim());
			}
		}
		String response = cityService.isTransportAvailabilityBetweenCities(origin,destination, cityMappings);
			
		return response;
	}
	
	public String getFileContent(String fileName) throws URISyntaxException {
		
		Path filePath = Paths.get(getClass().getClassLoader().getResource(fileName).toURI());
		log.info("filePath.."+filePath.getFileName()+"---"+filePath);
		
		byte[] fileBytes = null;
		try {
			fileBytes = Files.readAllBytes(filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}	
		
		log.info("Its a File so proceed to read the file contents..");
		String cityData = new String(fileBytes);
		log.info("<-------Loading city information ---->\n"+cityData);
		return cityData;
		
	}
	
}
