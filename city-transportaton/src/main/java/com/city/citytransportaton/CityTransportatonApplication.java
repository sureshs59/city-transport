package com.city.citytransportaton;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.city.controller", "com.city.service"})
public class CityTransportatonApplication {

	public static void main(String[] args) {
		SpringApplication.run(CityTransportatonApplication.class, args);
	}

}
