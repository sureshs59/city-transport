package com.city.citytransportaton;

import static org.junit.Assert.fail;

import java.net.URISyntaxException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.city.controller.CityTransportController;
import com.city.service.CityServiceInterface;

@SpringBootTest
class CityTransportatonApplicationTests {
	String cityInfomration = null;
	Logger logger = LogManager.getLogger(this.getClass());
	
	@Autowired
	private CityTransportController cityTransportController;
	
	@Autowired
	CityServiceInterface cityService;
	
	@Test
	void contextLoads() {
	}
	
	@Test
	public void loadFile() {
		try {
			logger.info("In Load file method and calling File content method..");
			String output = cityTransportController.getFileContent("city.txt");
			Assert.assertTrue( " File loaded success ", output.length() > 0);
		} catch (URISyntaxException e) {
			logger.info("Exception occured.."+e.getMessage());
			fail("Exception in testStoreData "+e);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCitiesWithNullReturn(){
		try{
			String origin 		= null;
			String destination  = null;
			String response = cityTransportController.getTransportationBetweenCities(origin, destination);
			Assert.assertNotNull(response);
		}catch(Exception e){
			logger.info("Exception occured.."+e.getMessage());
			fail("Exception in testStoreData "+e);
			e.printStackTrace();
		}
	}
	
	/*
	 * http://localhost:8080/connected?origin=Boston&destination=Newark
	   Should return yes
	*/
	
	@Test
	public void testCityInformationWithTestData(){
		try{
			String origin 		= "Boston";
			String destination  = "Newark";
			String response = cityTransportController.getTransportationBetweenCities(origin, destination);
			Assert.assertSame("YES", response);
		}catch(Exception e){
			logger.info("Exception occured.."+e.getMessage());
			fail("Exception in testStoreData "+e);
			e.printStackTrace();
		}
	}
	
	/*
	 * http://localhost:8080/connected?origin=Philadelphia&destination=Albany
	   should return no 
	*/
	@Test
	public void testCityInformationWithTestDataNegative(){
		try{
			String origin 		= "Philadelphia";
			String destination  = "Albany";
			String response = cityTransportController.getTransportationBetweenCities(origin, destination);
			Assert.assertSame("NO", response);
		}catch(Exception e){
			logger.info("Exception occured.."+e.getMessage());
			fail("Exception in testStoreData "+e);
			e.printStackTrace();
		}
	}
}
